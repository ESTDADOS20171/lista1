package q1 

object Questao1 extends App {

  println("Exemplo lista encadeada.\n");
  var L = new ListaEncadeada[Integer]();

  print("L = ");
  L.imprimeValores();

  println("Lista vazia = " + L.estaVazia() + "\n");

  print("L = ");
  L.inserir(3);
  L.imprimeValores();

  print("L = ");
  L.inserir(5);
  L.imprimeValores();

  print("L = ");
  L.inserir(2);
  L.imprimeValores();

  print("L = ");
  L.inserir(6);
  L.imprimeValores();

  print("L = ");
  L.inserir(1);
  L.imprimeValores();

  print("\nImprimir valores: ");
  L.imprimeValores();

  print("Imprimir valores recursao: ");
  L.imprimeValoresRecursao();

  print("Imprimir valores recursao reversa: ");
  L.imprimeValoresRecursaoReversa();

  println("\nLista L vazia ? = " + L.estaVazia());

  var J = new ListaEncadeada[Int]();

  println("Lista J vazia ? = " + J.estaVazia() + "\n");

  print("Busca: ");
  L.imprimeValores();

  println("Buscar 2 = " + L.buscar(2));
  println("Buscar 4 = " + L.buscar(4));

  print("\nRecuperar: ");
  L.imprimeValores();
  println("Recuperar 2 = " + L.recuperar(2));
  println("Recuperar 4 = " + L.recuperar(4));

  println("\nRemover (2)");
  print("Antes ");
  L.imprimeValores();
  L.remover(2);

  print("Depois ");
  L.imprimeValores();

  print("Remover Recursivo (6) ");
  L.removerRecursivo(6);
  L.imprimeValores();

  print("\nLiberar ");
  L.liberar();

  print("L = ");
  L.imprimeValores();

  println("Lista L vazia ? = " + L.estaVazia());

}