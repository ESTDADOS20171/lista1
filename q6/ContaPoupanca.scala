package q6

import scala.collection.mutable.LinkedHashMap

class ContaPoupanca(num: Int, s: Double, t: Double) extends ContaBancaria(num, s) {

  var taxa = t;

  def renderJuros() {
    var juros = taxa * saldo;
    credito(juros);
  }
  
    override def toString = "Conta Poupanca {#:" + numero + ", $:" + saldo + ", %:" + taxa + "}";


}