package q6

class ContaFidelidade(num: Int, s: Double) extends ContaBancaria(num, s) {

  protected var bonus = 0.0;
  
  def consultarBonus(): Double = bonus;

  override def credito(valor : Double): Unit = {
    bonus = math.abs(valor) * 0.01;
    super.credito(valor);
  }


  def renderBonus() {
    saldo = saldo + bonus;
    bonus = 0;
  }
  
  override def toString = "Conta Fidelidade {#:" + numero + ", $:" + saldo + ", B:" + bonus + "}";
}