package q6

import java.util.ArrayList
import java.io.File

class ContaBancaria(num: Int, sal: Double = 0) extends Ordered[ContaBancaria] {

  protected var numero = num;
  protected var saldo = sal;
  
  def consultarSaldo():Double = saldo;

  def credito(valor: Double) {
    saldo = saldo + math.abs(valor);
  }

  def debito(valor: Double) {

    saldo = saldo - math.abs(valor);
  }

  def transferir(conta: ContaBancaria, valor: Double): Boolean = {

    if (conta != null) {
      if (this.saldo > valor) {
        debito(valor)

        conta.credito(valor);
        return true;
      }
    }

    return false;
  }

  override def compare(that: ContaBancaria): Int = {
    return numero - that.numero;
  }
  
  override def equals(other: Any) = other match {
    case that: ContaBancaria => (this.numero == that.numero);
    case _ => false
  }
  
  override def toString = "Conta Bancaria {#:" + numero + ", $:" + saldo + "}";

}