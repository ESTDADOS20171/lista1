package q6

import q2.ListaEncadeadaOrdenada;

object Questao6 extends App {

  var l = new ListaEncadeadaOrdenada[ContaBancaria]();

  var a = new ContaBancaria(2, 11);
  
  var b = new ContaPoupanca(1, 90, 0.2);

  var c = new ContaFidelidade(4, 10);

  l.imprimeValores();
  
  println("Conta corrente A: " + a)
  println("Icluir conta corrente A");
  l.inserir(a);
  l.imprimeValores();
  
  println("Conta poupanca B: " + b)
  println("Icluir conta poupanca B");
  l.inserir(b);
  l.imprimeValores();
  
  println("Conta fidelidade C: " + c)
  println("Icluir conta fidelidade C");
  l.inserir(c);
  l.imprimeValores();
  
  
  println("Realizar credito de $ 10 na conta A");
  a.credito(10);
  l.imprimeValores();
  
  println("Realizar debito de $ 5 na conta B");
  b.debito(5);
  l.imprimeValores();
  
  println();
  println("Consultar saldo na conta B: $" + b.consultarSaldo());
  println("Consultar saldo na conta C: $" + c.consultarSaldo());
  
  println("Realizar transferencia de $15 entre B e C.");
  b.transferir(c, 15);

  println();
  println("Consultar saldo na conta B: $" + b.consultarSaldo());
  println("Consultar saldo na conta C: $" + c.consultarSaldo());
  println("Consultar bonus na conta C: $" + c.consultarBonus());
  
  
  println();
  
  println("Consultar saldo na conta B: $" + b.consultarSaldo());
  println("Render juros na conta B");
  b.renderJuros();
  println("Consultar saldo na conta B: $" + b.consultarSaldo());
  
  
  println();
  
  println("Conta fidelidade C: " + c)
  println("Render bonus na conta C");
  c.renderBonus();
  println("Conta fidelidade C: " + c)

  
  println();
  l.imprimeValores();
  l.remover(b); // remover conta com o numero 2
  println("Remover conta B");
  l.imprimeValores();
}