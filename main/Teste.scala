package main

import q1.Questao1
import q2.Questao2
import q3.Questao3
import q4.Questao4
import q6.Questao6
import q5.Questao5

object Teste extends App {

  println(" ======== QUESTAO 1 ========")

  Questao1.main(null);

  println(" ======== QUESTAO 2 ========")

  Questao2.main(null);
  
  println(" ======== QUESTAO 3 ========")

  Questao3.main(null);

  println(" ======== QUESTAO 4 ========")

  Questao4.main(null);

  println(" ======== QUESTAO 5 ========")

  Questao5.main(null);

  println(" ======== QUESTAO 6 ========")

  Questao6.main(null);
  

}