package q3

import q1.ListaEncadeada

class ListaDuplamenteEncadeadaOrdenada[E <% Ordered[E]] extends ListaEncadeada[E] {

  override def inserir(e: E): Boolean = {
    var no: No = new No(e, null, null)
    if (estaVazia() == 1) {

      cabeca = no;

    } else if (e < cabeca.v) {

      no.p = cabeca;
      no.a = null;

      cabeca.a = no;
      cabeca = no;
    } else {

      var iterador = cabeca;

      while (iterador != null) {

        if (iterador.p == null) {
          /** insere no final */
          iterador.p = no;
          no.a = iterador;
          no.p = null;

        } else if (e < iterador.v) {
          no.p = iterador;
          no.a = iterador.a;
          iterador.a.p = no;

        }

        iterador = iterador.p;
      }
    }

    return true;
  }

  override def remover(valor: E): E = {

    if (this.estaVazia() != 1) {

      //  println(cabeca);

      if (cabeca.v == valor) {
        cabeca = cabeca.p;
        cabeca.a = null;
      } else {
        var iterador = cabeca.p;

        while (iterador != null) {

          if (iterador.v == valor) {
            //   println(iterador);

            if (iterador.a != null) {
              iterador.a.p = iterador.p;
            }

            if (iterador.p != null) {
              iterador.p.a = iterador.a;
            }

            return valor;

          }

          iterador = iterador.p;

        }
      }
    }

    return valor;
  }

  override def removerRecursivo(valor: E, atual: No = null, comeco: No = cabeca): E = {

    if (this.estaVazia() != 1) {

      if (comeco == cabeca) {
        if (valor == cabeca.v) {

          cabeca = cabeca.p;
          cabeca.a = null;

          return valor;

        } else {
          // a flag indicando que nao e mais a partir da cabeca
          removerRecursivo(valor, cabeca.p, null);
        }
      }

      if (atual != null) {
        if (valor == atual.v) {

          if (atual.v == valor) {

            if (atual.a != null) {
              atual.a.p = atual.p;
            }

            if (atual.p != null) {
              atual.p.a = atual.a;
            }

            return valor;

          }

        }

        removerRecursivo(valor, atual.p, null);
      }

    }

    return valor;
  }

  def eIgual(outra: ListaDuplamenteEncadeadaOrdenada[E]): Boolean = {
    var iterador1 = cabeca;
    var iterador2 = outra.cabeca;

    while (iterador1 != null && iterador2 != null) {
      if (iterador1.v != iterador2.v) {
        return false;
      }

      iterador1 = iterador1.p;
      iterador2 = iterador2.p;
    }

    if (iterador1 != iterador2 && (iterador1 == null || iterador2 == null)) {
      return false;
    }

    return true;
  }
}
