package q3


object Questao3 extends App {

  println("Exemplo lista encadeada ordenada.\n");

  var L = new ListaDuplamenteEncadeadaOrdenada[Int]();
  print("L = ");
  L.imprimeValores();

  println("Lista vazia = " + L.estaVazia() + "\n");

  print("L = ");
  L.inserir(5);
  L.imprimeValores();

  print("L = ");
  L.inserir(3);
  L.imprimeValores();

  print("L = ");
  L.inserir(2);
  L.imprimeValores();

  print("L = ");
  L.inserir(6);
  L.imprimeValores();

  print("L = ");
  L.inserir(1);
  L.imprimeValores();

  print("\nImprimir valores: ");
  L.imprimeValores();

  print("Imprimir valores recursao: ");
  L.imprimeValoresRecursao();

  print("Imprimir valores recursao reversa: ");
  L.imprimeValoresRecursaoReversa();

  println("\nLista L vazia ? = " + L.estaVazia());

  var J = new ListaDuplamenteEncadeadaOrdenada[Int]();

  println("Lista J vazia ? = " + J.estaVazia() + "\n");

  print("Busca: ");
  L.imprimeValores();

  println("Buscar 2 = " + L.buscar(2));
  println("Buscar 4 = " + L.buscar(4));

  print("\nRecuperar: ");
  L.imprimeValores();
  println("Recuperar 2 = " + L.recuperar(2));
  println("Recuperar 4 = " + L.recuperar(4));

  println("\nRemover (2)");
  print("Antes ");
  L.imprimeValores();
  L.remover(2);

  print("Depois ");
  L.imprimeValores();

  println("Remover Recursivo (6) ");
  L.removerRecursivo(6);
  print("Depois ");
  L.imprimeValores();

  print("\nLiberar ");
  L.liberar();

  print("L = ");
  L.imprimeValores();

  println("Lista L vazia ? = " + L.estaVazia());

  var A = new ListaDuplamenteEncadeadaOrdenada[Int]();
  var B = new ListaDuplamenteEncadeadaOrdenada[Int]();

  println("\nLista A = B (A e B vazias) ? = " + A.eIgual(B));

  A.inserir(1);
  A.inserir(2);
  A.inserir(3);
  A.inserir(4);

  println("Lista A = B (A com elementos e B vazia) ? = " + A.eIgual(B));

  B.inserir(1);
  B.inserir(2);
  B.inserir(3);
  B.inserir(4);

  println("Lista A = B (mesmos elementos) ? = " + A.eIgual(B));

  B.remover(2);

  println("Lista A = B (elementos diferentes) ? = " + A.eIgual(B));

}