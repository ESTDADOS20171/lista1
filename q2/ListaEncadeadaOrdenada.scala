package q2

import q1.ListaEncadeada

class ListaEncadeadaOrdenada[E <% Ordered[E]] extends ListaEncadeada[E] {

  override def inserir(e: E): Boolean = {
    var no: No = new this.No(e, null)
    if (estaVazia() == 1) {
      cabeca = no;
    } else if(e < cabeca.v) {
      no.p = cabeca;
      cabeca = no;
    } else {
      var iterador = cabeca.p;
      var anterior = cabeca;

      while (iterador != null) {

        if (e < iterador.v) {
          no.p = iterador.p;
          anterior.p = no;
          return true;
        }

        anterior = iterador;
        iterador = iterador.p;
      }
      
      anterior.p = no;
      no.p = iterador;

    }
    return true;
  }

  
  def eIgual(outra: ListaEncadeadaOrdenada[E]) : Boolean = {
    var iterador1 = cabeca;
    var iterador2 = outra.cabeca;

    while (iterador1 != null && iterador2 != null ) {
      if (iterador1.v != iterador2.v) {
        return false;
      }
      
      iterador1 = iterador1.p;
      iterador2 = iterador2.p;
    }

    if(iterador1 != iterador2 && (iterador1 == null || iterador2 == null)) {
      return false;
    }
    
    
    return true;
  }
}
