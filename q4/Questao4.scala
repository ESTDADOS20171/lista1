package q4

object Questao4 extends App {
  var l = new ListaCircular[Int]();
  
  print("Criar lista circular vazia: L = ");
  l.imprimeValores();
  println();
  
  
  print("Inserir 2: L = ");
  l.inserir(2);
  l.imprimeValores();
  println();
  
  
  print("Inserir 3: L = ");
  l.inserir(3);
  l.imprimeValores();
  println();
  
  
  print("Inserir 4: L = ");
  l.inserir(4);
  l.imprimeValores();
  println();
  
  
  print("Inserir 6: L = ");
  l.inserir(6);
  l.imprimeValores();
  println();
  
  println("Imprimir valores");
  l.imprimeValores();
  println();
  
   println("Imprimir valores recursivamente");
  l.imprimeValoresRecursao();
  println();
  
  println("Lista vazia = " + l.estaVazia() + "\n");
  
  
  var g = new ListaCircular[Int]();
  
  print("Criar lista circular vazia: G = ");
  println("Lista vazia = " + g.estaVazia() + "\n");

  
  print("Busca: ");
  l.imprimeValores();

  println("Buscar 1 = " + l.buscar(1));
  println("Buscar 3 = " + l.buscar(3));

  print("\nRecuperar: ");
  l.imprimeValores();
  println("Recuperar 1 = " + l.recuperar(1));
  
  try {
    println("Recuperar 3 = " + l.recuperar(3));  
  } catch {
    case t: Throwable => println("Nao existe 3 em L.");
  }
  
  
  println("Imprimir valores");
  l.imprimeValores();
  println();
  
  println("Remover 3 de L.");
  l.remover(3);

  l.imprimeValores();
  println();
  
  println("Remover 4 de L recursivamente.");
  l.remover(4);

  l.imprimeValores();
  println();
  
  println("Lista vazia = " + l.estaVazia());
  println("Liberar lista L");
  l.liberar();
  println("Lista vazia = " + l.estaVazia() + "\n");
  
}