package q4

import q1.ListaEncadeada

class ListaCircular[E <% Ordered[E]] extends ListaEncadeada[E] {

  protected var cauda: No = null;

  override def inserir(valor: E): Boolean = {
    var no: No = new No(valor, null);
    if (this.estaVazia() == 1) {
      cabeca = no;
      cauda = no;
    } else {
      cauda.p = no;
      cauda = no;
    }

    return true;

  }

  override def remover(valor: E): E = {

    if (this.estaVazia() != 1) {

      if (cabeca.v == valor) {

        cabeca = cabeca.p;

      } else {

        var iterador: this.No = cabeca;
        var anterior: this.No = cabeca;

        while (iterador != null) {
          if (iterador.v == valor) {
            anterior.p = iterador.p;

            if (iterador.p == null) {
              cauda = anterior;
            }

            return valor;
          }

          anterior = iterador;
          iterador = iterador.p;

        }
      }

    }

    return valor;
  }

  override def removerRecursivo(valor: E, anterior: No = null, atual: No = null): E = {

    if (this.estaVazia() != 1) {

      if (valor == cabeca.v) {

        cabeca = cabeca.p;

      } else if (atual != null) {

        if (valor == atual.v) {
         println("ACHOU " + valor) 
          anterior.p = atual.p;
          
          if (atual.p == null) {
            cauda = anterior;
          }
          
        } else {
          
          removerRecursivo(valor, atual, atual.p);
          
        }
        
      }

    }

    return valor;
  }
}