package q5

import q4.ListaCircular

class ListaCircularDuplamenteEncadeada[E <% Ordered[E]] extends ListaCircular[E] {

  override def inserir(valor: E): Boolean = {
    var no: No = new No(valor, null, null);
    if (this.estaVazia() == 1) {
      cabeca = no;
      cauda = no;
    } else {
      no.a = cauda;
      cauda.p = no;
      cauda = no;
    }

    return true;

  }

  override def remover(valor: E): E = {

    if (this.estaVazia() != 1) {

      if (cabeca.v == valor) {

        cabeca = cabeca.p;

      } else {

        var iterador: this.No = cabeca.p;

        while (iterador != null) {
          if (iterador.v == valor) {

            iterador.a.p = iterador.p;

            if (iterador.p == null) {
              cauda = iterador.a;
            } else {
              iterador.p.a = iterador.a;
            }

            return valor;
          }

          iterador = iterador.p;

        }
      }

    }

    return valor;
  }

  override def removerRecursivo(valor: E, anterior: No = null, atual: No = null): E = {

    if (this.estaVazia() != 1) {

      if (valor == cabeca.v) {

        cabeca = cabeca.p;

      } else if (atual != null) {

        if (valor == atual.v) {
          anterior.p = atual.p;

          if (atual.p == null) {
            cauda = anterior;
          }

        } else {

          removerRecursivo(valor, atual, atual.p);

        }

      }

    }

    return valor;
  }
}