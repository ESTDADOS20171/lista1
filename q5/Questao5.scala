package q5

object Questao5 extends App {
   var l = new ListaCircularDuplamenteEncadeada[Int]();
  
  print("Criar lista circular vazia: L = ");
  l.imprimeValores();
  println();
  
  
  print("Inserir 1: L = ");
  l.inserir(1);
  l.imprimeValores();
  println();
  
  
  print("Inserir 3: L = ");
  l.inserir(3);
  l.imprimeValores();
  println();
  
  
  print("Inserir 5: L = ");
  l.inserir(5);
  l.imprimeValores();
  println();
  
  
  print("Inserir 9: L = ");
  l.inserir(9);
  l.imprimeValores();
  println();
  
  println("Imprimir valores");
  l.imprimeValores();
  println();
  
   println("Imprimir valores recursivamente");
  l.imprimeValoresRecursao();
  println();
  
  println("Lista vazia = " + l.estaVazia() + "\n");
  
  
  var g = new ListaCircularDuplamenteEncadeada[Int]();
  
  print("Criar lista circular vazia: G = ");
  println("Lista vazia = " + g.estaVazia() + "\n");

  
  print("Busca: ");
  l.imprimeValores();

  println("Buscar 1 = " + l.buscar(1));
  println("Buscar 3 = " + l.buscar(3));

  print("\nRecuperar: ");
  l.imprimeValores();
  println("Recuperar 1 = " + l.recuperar(1));
  
  try {
    println("Recuperar 3 = " + l.recuperar(3));  
  } catch {
    case t: Throwable => println("Nao existe 3 em L.");
  }
  
  
  println("Imprimir valores");
  l.imprimeValores();
  println();
  
  println("Remover 3 de L.");
  l.remover(3);

  l.imprimeValores();
  println();
  
  println("Remover 5 de L recursivamente.");
  l.remover(5);

  l.imprimeValores();
  println();
  
  println("Lista vazia = " + l.estaVazia());
  println("Liberar lista L");
  l.liberar();
  println("Lista vazia = " + l.estaVazia() + "\n");
}